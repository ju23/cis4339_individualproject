json.array!(@diagnostics) do |diagnostic|
  json.extract! diagnostic, :id, :code, :description, :fee
  json.url diagnostic_url(diagnostic, format: :json)
end
