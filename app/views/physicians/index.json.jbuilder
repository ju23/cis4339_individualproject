json.array!(@physicians) do |physician|
  json.extract! physician, :id, :physicianname, :physicianaddress, :physicianphone
  json.url physician_url(physician, format: :json)
end
