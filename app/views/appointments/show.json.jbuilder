json.extract! @appointment, :id, :patient_id, :physician_id, :date, :reason, :notes, :diagnostic_id, :created_at, :updated_at
