json.array!(@patients) do |patient|
  json.extract! patient, :id, :patientname, :patientaddress, :patientphone
  json.url patient_url(patient, format: :json)
end
