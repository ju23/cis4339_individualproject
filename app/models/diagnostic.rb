class Diagnostic < ActiveRecord::Base

  has_one :appointment


  validates :fee, numericality:  {:greater_than_or_equal_to => 0,
                                  :message => " Only positive numbers are allowed" }


end
