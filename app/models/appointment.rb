class Appointment < ActiveRecord::Base

  belongs_to :physician
  belongs_to :patient
  belongs_to :diagnostic
  belongs_to :status

  validates  :date, uniqueness: true

  #validate :day_valid, :before => :create

  validate :cancel, :before => :delete



#   private
#   def day_valid
#     if self.date.saturday? or date.sunday?
#       errors.add(:date, 'Please choose a weekday')
#
#
#     end
#     end
# end

private
def cancel
  return true if self.date?
  ((date - DateTime.current) / 3600) > 8
  errors.add(:date, 'Cannot Cancel Appointment')
else return false
end
end




