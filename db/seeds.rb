# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


require 'csv'
Diagnostic.delete_all
open("/vagrant/projects/Cis4339Individual/cd.csv") do |c|
  c.read.each_line do |c|
    code, description, fee = c.chomp.split(",")
    Diagnostic.create!(code: code, description: description, fee: fee)
  end
end



statuses = Status.create([{name: 'Complete'},
                           {name: 'Pending'},
                           {name: 'In Progress'},
                           {name: 'Sign in'}])


physicians = Physician.create([{ physicianname: 'Carlos Granada',physicianaddress: '24563 Huck Drive', physicianphone: '713-890-7654' },
                               { physicianname: 'Tyler Creator', physicianaddress: '5003 South Street', physicianphone: '345-987-9043' },
                               { physicianname: 'Star Shelly', physicianaddress: '1456 WestHill Ave', physicianphone: '832-876-5432' },
                               { physicianname: 'Dwayne Young', physicianaddress: '0045 Purple street', physicianphone: '234-321-7777' },
                               { physicianname: 'Beyonce Carter', physicianaddress: '500 Silent Hill', physicianphone: '832-555-2312' }])

patients = Patient.create([{ patientname: 'Jay Bustos', patientaddress: '243 Huckleberry Drive', patientphone: '281-456-8900' },
                           { patientname: 'Tyrese Gibbs', patientaddress: '5523 SouthPark Street', patientphone: '345-567-9765' },
                           { patientname: 'Perla Shelton', patientaddress: '1390 WestBerry Ave', patientphone: '123-567-5555'},
                           { patientname: 'Diane Yoo', patientaddress: '145 Red Ave', patientphone: '345-678-9008' },
                           { patientname: 'Bobby Hill', patientaddress: '23500 King Hill', patientphone: '567-444-5678' }])

