class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :patientname
      t.string :patientaddress
      t.string :patientphone

      t.timestamps
    end
  end
end
