class CreateDiagnostics < ActiveRecord::Migration
  def change
    create_table :diagnostics do |t|
      t.string :code
      t.string :description
      t.float :fee

      t.timestamps
    end
  end
end
