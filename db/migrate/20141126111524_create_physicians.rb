class CreatePhysicians < ActiveRecord::Migration
  def change
    create_table :physicians do |t|
      t.string :physicianname
      t.string :physicianaddress
      t.string :physicianphone

      t.timestamps
    end
  end
end
